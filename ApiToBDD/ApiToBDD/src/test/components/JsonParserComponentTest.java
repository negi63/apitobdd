package test.components;

import ProjetDevOps.ApiToBDD.components.DataBaseComponent;
import ProjetDevOps.ApiToBDD.components.JsonParserComponent;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class JsonParserComponentTest {
    private final DataBaseComponent dataBaseComponent = new DataBaseComponent();
    private final JsonParserComponent jsonParserComponent = new JsonParserComponent(dataBaseComponent);


    @Test
    void snakeCasetest() {
        assertEquals("code_postaux", jsonParserComponent.snakeCase("codePostaux"));
        assertNotEquals("codeid", jsonParserComponent.snakeCase("codeId"));
    }

    @Test
    void addIdInTableTest() {
        assertEquals("ALTER table regions ADD COLUMN id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST; \n",
                jsonParserComponent.addIdInTable("regions"));
    }

}
