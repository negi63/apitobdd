package ProjetDevOps.ApiToBDD.repositories;

import ProjetDevOps.ApiToBDD.entities.ApiBDD;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiBDDRepository extends JpaRepository<ApiBDD, Long> {
}
