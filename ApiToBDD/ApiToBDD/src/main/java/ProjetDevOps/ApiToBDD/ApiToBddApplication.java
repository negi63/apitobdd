package ProjetDevOps.ApiToBDD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiToBddApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiToBddApplication.class, args);
    }

}
