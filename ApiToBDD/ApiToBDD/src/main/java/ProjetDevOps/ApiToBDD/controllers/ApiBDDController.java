package ProjetDevOps.ApiToBDD.controllers;

import ProjetDevOps.ApiToBDD.components.ApiBddComponent;
import ProjetDevOps.ApiToBDD.entities.ApiBDD;
import ProjetDevOps.ApiToBDD.services.ApiBDDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.SQLException;
import java.util.Date;

@Controller
public class ApiBDDController {
    private final ApiBDDService apiBDDService;
    private final ApiBddComponent apiBddComponent;

    @Autowired
    public ApiBDDController(ApiBDDService apiBDDService, ApiBddComponent apiBddComponent) {
        this.apiBDDService = apiBDDService;
        this.apiBddComponent = apiBddComponent;
    }

    @GetMapping("/")
    public String showForm(Model model) {
        model.addAttribute("apiBDD",  new ApiBDD());
        return "index";
    }

    @PostMapping("/")
    public String submitForm(@ModelAttribute("apiBDD") ApiBDD apiBDD, Model model) throws SQLException {
        apiBDD.setCreatedAt(new Date());
        apiBDD.setDburl("jdbc:" + apiBDD.getDburl());
        this.apiBDDService.save(apiBDD);
        this.apiBddComponent.setApiBdd(apiBDD);
        String info = this.apiBddComponent.startTreatment();
        model.addAttribute("info", info);
        return "form_success";
    }
}
