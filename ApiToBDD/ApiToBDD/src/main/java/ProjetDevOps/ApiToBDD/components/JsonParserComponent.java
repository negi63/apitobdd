package ProjetDevOps.ApiToBDD.components;

import ProjetDevOps.ApiToBDD.entities.ApiBDD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class JsonParserComponent {

    private final DataBaseComponent dataBaseComponent;

    private ApiBDD apiBDD;

    private String createQuery = "";
    private String dropTable = "";
    private String insertQuery = "";


    @Autowired
    public JsonParserComponent(DataBaseComponent dataBaseComponent) {
        this.dataBaseComponent = dataBaseComponent;
    }

    private void reset() {
        this.createQuery = "";
        this.dropTable = "";
        this.insertQuery = "";
    }

    public void treatement(String jsonContent, ApiBDD apiBDD) throws SQLException {
        this.reset();
        JSONTokener jsonTokener = new JSONTokener(jsonContent);
        this.apiBDD = apiBDD;
        Map<String, String> fields;
        List<String> constrains = new ArrayList<>();
        if (jsonContent.startsWith("[")) {
            fields = this.jsonArrayTreatment(jsonTokener);
        } else {
            fields = this.jsonObjectTreatment(new JSONObject(jsonTokener));
        }

        String table = this.snakeCase(this.getTableName());
        List<String> columns = this.createTable(table, fields, constrains);
        this.dropTable = this.dropTable.replace("\n", "").replace("\t", "");
        this.createQuery = this.createQuery.replace("\n", "").replace("\t", "");

        String[] querydrops = this.dropTable.split(";");
        for (String query : querydrops) {
            System.out.println(query);
            this.executeQuery(query);
        }


        String[] querycreate = this.createQuery.split(";");
        for (String query : querycreate) {
            System.out.println(query);
            this.executeQuery(query);
        }
//        System.out.println(this.dropTable);
//        System.out.println(this.createQuery);
//        this.executeQuery(this.dropTable);
//        this.executeQuery(this.createQuery);

        if (jsonContent.startsWith("[")) {
            this.insertJsonArray(table, new JSONTokener(jsonContent), columns);
        } else {
            StringBuilder query = new StringBuilder("INSERT INTO " + table + " (");
            Map<String, Object> mapJson = new JSONObject(new JSONTokener(jsonContent)).toMap();
            for (String key : mapJson.keySet()) {
                if (!(mapJson.get(key) instanceof ArrayList)) {
                    key = this.snakeCase(key);
                    query.append(key).append(", ");
                }
            }

            query = new StringBuilder(query.substring(0, query.length() - 2));
            query.append(" ) VALUES ");
            query.append(this.insertTableValues(table, new JSONObject(new JSONTokener(jsonContent)), columns));
            query.append("; \n");
            this.insertQuery += query;
        }
        String[] queryinsert = this.insertQuery.split(";");
        for (String query : queryinsert) {
            System.out.println(query);
            this.executeQuery(query);
        }
    }

    private Map<String, String> jsonObjectTreatment(JSONObject jsonObj) {
        Map<String, String> fields = new HashMap<>();
        Map<String, Object> mapJson = jsonObj.toMap();
        for (String key : mapJson.keySet()) {
            String valueClassType = mapJson.get(key).getClass().getSimpleName();
            if (valueClassType.equals("String")) {
                if (mapJson.get(key).toString().length() > 255) {
                    fields.put(key, "TEXT DEFAULT NULL");
                } else {
                    fields.put(key, "VARCHAR(255) DEFAULT NULL");
                }
            } else if (valueClassType.equals("Integer")) {
                fields.put(key, "INT(10) DEFAULT NULL");
            } else if (valueClassType.equals("Boolean")) {
                fields.put(key, "TINYINT(1)  DEFAULT NULL");
            } else if (valueClassType.equals("HashMap")) {
                //this.jsonObjectTreatment(jsonObj.getJSONObject(key));
                System.out.println(key);
            } else if (valueClassType.equals("ArrayList")) {
                JSONArray array = jsonObj.getJSONArray(key);
                System.out.println(key);
                String foreignKey = this.snakeCase(this.getTableName()) + "_id";
                this.dropTable += "DROP TABLE IF EXISTS " + this.snakeCase(key) + "; \n";
                String createArray = "CREATE TABLE "
                        + this.snakeCase(key)
                        + " (\n\tid int(10) AUTO_INCREMENT PRIMARY KEY,\n\tlabel ";
                String valueArraytype = array.get(0).getClass().getSimpleName();
                if (valueArraytype.equals("String")) {
                    createArray += "VARCHAR(255) DEFAULT NULL,\n";
                } else if (valueArraytype.equals("Integer")) {
                    createArray += "INT(10) DEFAULT NULL,\n";
                } else if (valueArraytype.equals("Boolean")) {
                    createArray += "TINYINT(1)  DEFAULT NULL,\n";
                }

                createArray += foreignKey + " INT(10),";

                createArray += " FOREIGN KEY ("
                        + foreignKey
                        + ") REFERENCES "
                        + this.snakeCase(this.getTableName())
                        + "("
                        + "id"
                        + ")";
                createArray += this.dataBaseComponent.getEngine();
                this.createQuery += createArray;
            }
            if (key.equals("id")) {

                if (valueClassType.equals("String")) {

                    fields.put(key, "VARCHAR(255) PRIMARY KEY");
                } else if (valueClassType.equals("Integer")) {

                    fields.put(key, "INT(10) AUTO_INCREMENT PRIMARY KEY");
                }

            }
        }
        return fields;
    }

    private Map<String, String> jsonArrayTreatment(JSONTokener jsonTokener) {
        JSONArray jsonArray = new JSONArray(jsonTokener);
        JSONObject jsonObj = jsonArray.getJSONObject(0);
        return this.jsonObjectTreatment(jsonObj);
    }

    public List<String> createTable(String name, Map<String, String> fields, List<String> constraints) {
        List<String> columns = new ArrayList<>();
        this.dropTable += "DROP TABLE IF EXISTS " + name + "; \n";
        String create = "CREATE TABLE " + name + " (\n";
        for (String key : fields.keySet()) {
            columns.add(key);
            create += "\t" + this.snakeCase(key) + " " + fields.get(key) + ",\n";
        }
        for (String constrain : constraints) {
            create += constrain + ",";
        }
        create = create.substring(0, create.length() - 2);
        create += this.dataBaseComponent.getEngine();
        String subQuery = create;

        if (!fields.containsKey("id")) {
            subQuery += this.addIdInTable(name);
        }
        this.createQuery = subQuery + this.createQuery;
        return columns;
    }

    public String addIdInTable(String table) {
        StringBuilder request = new StringBuilder();
        request.append("ALTER table");
        request.append(" ");
        request.append(table);
        request.append(" ");
        request.append("ADD COLUMN id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST; \n");
        return request.toString();
    }

    private StringBuilder insertTableValues(String name, JSONObject jsonObj, List<String> columns) {
        StringBuilder query = new StringBuilder("(");
        int i = 0;
        for (String column : columns) {
            if (i != 0) {
                query.append(", ");
            }
            if (jsonObj.has(column)) {
                if (jsonObj.get(column) instanceof String) {
                    if (((String) jsonObj.get(column)).contains("\"")) {
                        String replacecot = jsonObj.get(column).toString();
                        replacecot = replacecot.replace("\"", "\'");
                        query.append("\"").append(replacecot).append("\"");
                    } else {
                        query.append("\"").append(jsonObj.get(column)).append("\"");
                    }
                } else {
                    query.append(jsonObj.get(column));
                }
            } else {
                query.append("null");
            }
            i++;
        }
        query.append(")");
        // System.out.println(query);
        return query;
    }

    private void insertJsonArray(String name, JSONTokener jsonTokener, List<String> columns) throws SQLException {
        JSONArray jsonArray = new JSONArray(jsonTokener);
        StringBuilder insertQuery = new StringBuilder("INSERT INTO " + name + " (");
        Map<String, Object> mapJson = jsonArray.getJSONObject(0).toMap();
        ArrayList<String> listKey = new ArrayList<>();
        for (String key : mapJson.keySet()) {
            System.out.println(mapJson.get(key).getClass()
                    .getSimpleName());
            if ((mapJson.get(key) instanceof ArrayList)) {
                listKey.add(key);
            } else if ((mapJson.get(key) instanceof HashMap)) {
                System.out.println("Not hashmap now");
            } else {

                key = this.snakeCase(key);
                insertQuery.append(key).append(", ");
            }
        }
        insertQuery = new StringBuilder(insertQuery.substring(0, insertQuery.length() - 2));
        insertQuery.append(") VALUES ");

        List<String> queries = new ArrayList<>();
        List<String> subQueries = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObj = jsonArray.getJSONObject(i);
            StringBuilder query = new StringBuilder();
            query.append(this.insertTableValues(name, jsonObj, columns));
            query.append(";");
            queries.add(query.toString());

            for (String arrayKey : listKey) {
                JSONArray subJsonArray = jsonObj.getJSONArray(arrayKey);
                for (int j = 0; j < subJsonArray.length(); j++) {
                    String insert = "INSERT INTO " + this.snakeCase(arrayKey) + " (id, label, " + this.snakeCase(name) + "_id) VALUES (NULL,";
                    if (subJsonArray.get(j) instanceof String) {
                        insert += "'" + subJsonArray.get(j) + "'";
                    } else {
                        insert += subJsonArray.get(j);
                    }
                    insert += ", (SELECT id FROM " + this.snakeCase(name) + " WHERE ";
                    for (String column : columns) {
                        if (jsonObj.has(column)) {
                            insert += this.snakeCase(column) + " = ";
                            if (jsonObj.get(column) instanceof String) {
                                insert += "\"" + jsonObj.get(column) + "\"" + " AND ";
                            } else {
                                insert += jsonObj.get(column) + " AND ";
                            }
                        }
                    }
                    insert = insert.substring(0, insert.length() - 5);
                    insert += "));";
                    subQueries.add(insert);
                }
            }
        }
        System.out.println(insertQuery);

        dataBaseComponent.executeBatch(
                insertQuery.toString(),
                queries,
                this.apiBDD.getDburl(),
                this.apiBDD.getDbuser(),
                this.apiBDD.getDbpassword())
        ;

        if (subQueries.size() > 0) {
            dataBaseComponent.executeBatch(
                    "",
                    subQueries,
                    this.apiBDD.getDburl(),
                    this.apiBDD.getDbuser(),
                    this.apiBDD.getDbpassword())
            ;
        }
    }

    private String getTableName() {
        String[] urlSplit = this.apiBDD.getUrl().split("/");
        return urlSplit[urlSplit.length - 1];
    }

    private void executeQuery(String query) {
        this.dataBaseComponent.executeQuery(query, this.apiBDD.getDburl(), this.apiBDD.getDbuser(), this.apiBDD.getDbpassword());
    }

    public String snakeCase(String str) {
        String result = "";
        char c = str.charAt(0);
        result = result + Character.toLowerCase(c);
        for (int i = 1; i < str.length(); i++) {

            char ch = str.charAt(i);

            if (Character.isUpperCase(ch)) {
                result = result + '_';
                result = result + Character.toLowerCase(ch);
            } else {
                result = result + ch;
            }
        }

        return result;
    }
}


