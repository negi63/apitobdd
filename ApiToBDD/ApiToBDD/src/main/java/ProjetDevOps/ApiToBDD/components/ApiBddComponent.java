package ProjetDevOps.ApiToBDD.components;

import ProjetDevOps.ApiToBDD.entities.ApiBDD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.http.HttpResponse;
import java.sql.SQLException;

@Component
public class ApiBddComponent {

    private final HttpComponent httpComponent;

    private final DataBaseComponent dataBaseComponent;

    private final JsonParserComponent jsonParserComponent;

    private ApiBDD apiBDD;

    @Autowired
    public ApiBddComponent(
            HttpComponent httpComponent,
            DataBaseComponent dataBaseComponent,
            JsonParserComponent jsonParserComponent
    ) {
        this.httpComponent = httpComponent;
        this.dataBaseComponent = dataBaseComponent;
        this.jsonParserComponent = jsonParserComponent;
    }

    public String startTreatment() throws SQLException {
        String jsonContent = getResponseContent();
        String message = "All good my frend";
        if (jsonContent != null) {
            if (jsonContent.contains("Cannot GET")) {
                message = "Erreur de l'URL de l'API";
            } else {
                this.jsonParserComponent.treatement(jsonContent, apiBDD);
            }
        }
        return message;
    }

    public void setApiBdd(ApiBDD apiBDD) {
        this.apiBDD = apiBDD;
    }

    private String getResponseContent() {
        HttpResponse<String> responseJson = null;
        try {
            responseJson = this.httpComponent.getResponseByUrl(apiBDD.getUrl());
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        if (responseJson != null) {
            return responseJson.body();
        }
        return null;
    }
}
