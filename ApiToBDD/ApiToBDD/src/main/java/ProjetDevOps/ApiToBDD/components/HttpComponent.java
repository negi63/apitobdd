package ProjetDevOps.ApiToBDD.components;

import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class HttpComponent {
    //permet d'envoyer la requette HTTP
    public HttpResponse<String> getResponseByUrl(String url) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(url))
                .build();
        HttpClient client = HttpClient.newHttpClient();

        try {
            return client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            System.out.println("An issue occured while trying to fetch the API...");
            System.out.println(e.getMessage());
            return null;
        }
    }
}
