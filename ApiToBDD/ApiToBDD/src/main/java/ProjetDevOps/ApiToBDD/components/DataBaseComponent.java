package ProjetDevOps.ApiToBDD.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.PrintStream;
import java.sql.*;
import java.util.List;

@Component
public class DataBaseComponent {
    private Statement statement;

//Génère la connection
    public void connect(String url, String user, String pwd) {
        try {
            Connection connection = DriverManager.getConnection(url, user, pwd);
            this.statement = connection.createStatement();

        } catch (Exception e) {
            System.out.println("Error while trying to access the DB : " + e.getMessage());
        }
    }
//permet de generer la DataBase
    public void genrateDB(String url, String user, String pwd, String DBname) {

        try {
            String sql = "CREATE DATABASE " + DBname;
            executeQuery(sql, url, user, pwd);

        } catch (Exception e) {
            System.out.println("Error while trying to access the DB : " + e.getMessage());
        }
    }
// permet d'envoyer les requêtes sql
    public void executeQuery(String query, String url, String user, String pwd) {
        try {
            connect(url, user, pwd);
            this.statement.execute(query);
            //System.out.println("Query : " + query + " - DONE");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
//permet d'envoyer plusieurs requêtes
    public void executeBatch(String insert, List<String> queries, String url, String user, String pwd) throws SQLException {
        final int batchSize = 1000;
        Connection conn = DriverManager.getConnection(url, user, pwd);
        Statement ps = null;
        try {
            ps = conn.createStatement();

            int insertCount = 0;
            for (String item : queries) {
                ps.addBatch(insert + item);
                if (++insertCount % batchSize == 0) {
                    ps.executeBatch();
                    System.out.println("1000 requests has been sent");
                }
            }
            ps.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
        finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String disableFK() {
        return "SET FOREIGN_KEY_CHECKS = 0;\n";
    }

    public String enableFK() {
        return "SET FOREIGN_KEY_CHECKS = 1;\n";
    }

    public String disableKeys(String table) {
        return "ALTER TABLE " + table + " DISABLE KEYS;\n";
    }

    public String getEngine() {
        return "\n) ENGINE = InnoDB;\n";
    }
}

