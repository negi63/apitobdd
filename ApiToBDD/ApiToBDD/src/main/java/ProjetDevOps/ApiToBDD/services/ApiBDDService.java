package ProjetDevOps.ApiToBDD.services;

import ProjetDevOps.ApiToBDD.entities.ApiBDD;
import ProjetDevOps.ApiToBDD.repositories.ApiBDDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApiBDDService {
    private final ApiBDDRepository apiBDDRepository;

    @Autowired
    public ApiBDDService(ApiBDDRepository apiBDDRepository) {
        this.apiBDDRepository = apiBDDRepository;
    }

    public void save(ApiBDD apiBDD) {
        this.apiBDDRepository.save(apiBDD);
    }
}
